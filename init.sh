#!/bin/sh

cd "$(dirname "$0")"

git config core.filemode false

rm -rf "react_web/src"
git clone git@gitlab.com:connectique-ndi-2021/unity-game-build.git src

chmod +x deploy.sh

./deploy.sh
